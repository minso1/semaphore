package semaphore_test

import (
	"fmt"

	"bitbucket.org/minso1/semaphore"
)

// Example_workerPool demonstrates how to use a semaphore to limit the number of
// goroutines working on parallel tasks.
func Example_workerPool() {

	var (
		maxWorkers = 8
		sem        = semaphore.New(maxWorkers)
		output     = make([]int, 32)
	)

	for i := range output {
		sem.Acquire()

		go func(i int) {
			defer sem.Release()
			output[i] = collatzSteps(i + 1)
		}(i)
	}

	// Drain the queue to make sure that all goroutines finish. Not needed if already
	// wrapped in errgroup or similar.
	sem.Drain()

	fmt.Println(output)

	// Output:
	// [0 1 7 2 5 8 16 3 19 6 14 9 9 17 17 4 12 20 20 7 7 15 15 10 23 10 111 18 18 18 106 5]
}

// collatzSteps computes the number of steps to reach 1 under the Collatz
// conjecture. (See https://en.wikipedia.org/wiki/Collatz_conjecture.)
func collatzSteps(n int) (steps int) {
	if n <= 0 {
		panic("nonpositive input")
	}

	for ; n > 1; steps++ {
		if steps < 0 {
			panic("too many steps")
		}

		if n%2 == 0 {
			n /= 2
			continue
		}

		const maxInt = int(^uint(0) >> 1)
		if n > (maxInt-1)/3 {
			panic("overflow")
		}
		n = 3*n + 1
	}

	return steps
}
