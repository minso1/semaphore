package semaphore

/*
Naive implementation of a semaphore to limit number of concurrent
tasks due to resource limitations.

See:
- https://en.wikipedia.org/wiki/Semaphore_(programming)
- https://pkg.go.dev/golang.org/x/sync/semaphore

*/
type Semaphore struct {
	ch chan struct{}
}

func New(capacity int) *Semaphore {
	// Make sure that capacity is at least 1
	if capacity < 1 {
		capacity = 1
	}

	return &Semaphore{ch: make(chan struct{}, capacity)}
}

// Reserve a slot
func (s *Semaphore) Acquire() {
	s.ch <- struct{}{}
}

// Release a slot
func (s *Semaphore) Release() {
	<-s.ch
}

// Naively drain all slots in the queue by first acquiring all and then
// releasing them. Does not take into account if more items are added while
// already draining
func (s *Semaphore) Drain() {
	capacity := cap(s.ch)

	// First acquire all slots to make sure all is empty
	for i := 0; i < capacity; i++ {
		s.Acquire()
	}

	// Then, release all of them to be able to qeueu stuff again
	for i := 0; i < capacity; i++ {
		s.Release()
	}
}
