# README

Naive implementation of a semaphore to limit number of concurrent
tasks due to resource limitations.

See:

- https://en.wikipedia.org/wiki/Semaphore_(programming)
- https://pkg.go.dev/golang.org/x/sync/semaphore

Note that this approach is a bit slower than the implementation linked above but our current needs does not need that amount of performance and this way we can configure the API after our needs.

## TODO:

- Add context to allow for timeouts etc
